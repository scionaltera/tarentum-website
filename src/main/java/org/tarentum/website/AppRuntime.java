package org.tarentum.website;

import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.naming.InitialContext;
import java.io.InputStream;
import java.util.jar.Attributes;
import java.util.jar.Manifest;

@Component
public class AppRuntime {
    private static final Logger log = LoggerFactory.getLogger(AppRuntime.class);

    private static String version = "SNAPSHOT";
    private static long bootTime = System.currentTimeMillis();
    private static String apiKey;
    private static String apiSecret;
    private static String callbackUrl;

    @Autowired
    private ApplicationContext applicationContext;

    @PostConstruct
    private void discoverVersion() {
        try {
            Resource resource = applicationContext.getResource("/META-INF/MANIFEST.MF");
            InputStream stream = resource.getInputStream();
            Manifest manifest = new Manifest(stream);
            Attributes attributes = manifest.getMainAttributes();
            String tmpVersion = attributes.getValue("Core-Version");

            if (tmpVersion != null && !tmpVersion.equals("")) {
                version = tmpVersion.replace("null", "SNAPSHOT");
            }

            log.info("Core Version: {}", version);
        } catch (Exception ex) {
            log.warn("Unable to read version information from manifest.", ex);
        }
    }

    @PostConstruct
    private void loadOAuthProperties() {
        try {
            InitialContext ic = new InitialContext();
            apiKey = (String)ic.lookup("java:comp/env/api-key");
            apiSecret = (String)ic.lookup("java:comp/env/api-secret");
            callbackUrl = (String)ic.lookup("java:comp/env/callback-url");

            log.info("Loaded OAuth properties.");
        } catch (Exception ex) {
            log.error("Unable to load OAuth properties!", ex);
        }
    }

    @NotNull
    public static String getVersion() {
        return version;
    }

    public static long getBootTime() {
        return bootTime;
    }

    @NotNull
    public static String getApiKey() {
        return apiKey;
    }

    @NotNull
    public static String getApiSecret() {
        return apiSecret;
    }

    @NotNull
    public static String getCallbackUrl() {
        return callbackUrl;
    }
}
