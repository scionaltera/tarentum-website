# Description
[![Build Status](https://drone.io/bitbucket.org/scionaltera/tarentum-website/status.png)](https://drone.io/bitbucket.org/scionaltera/tarentum-website/latest)

This is the code for the [Clan Tarentum][tarentum-website] website. It is a part of the [Dark Jedi Brotherhood][djb], a free online Star Wars club featuring writing, gaming and other Star Wars themed activities.

The site uses JSP, Spring, and Hibernate and we have attempted to keep everything as simple and standard as possible.

## Local Installation
The website follows the typical Maven structure and is designed to be easy to set up locally for testing. You will need the following tools installed to run the site locally:

1. Java 7 or later.
2. Maven 3.1.1 or later.
3. A MySQL database (for the default configuration, but Hibernate should work with any other major database with a little extra configuration).

Edit the database settings in `src/main/config/jetty-env.xml` to match your database setup. You just need the user and the database for the initial setup. Hibernate will automatically create the necessary tables when you start the site up for the first time.

To start up the site, you just need to run the following command from the command line:

    mvn jetty:run-war

After running the command, Maven will download the appropriate version of the Jetty web server, all the dependencies, compile the code, run the tests, connect to the database and finally start up the web server for you. You can visit the local version of the site at `http://localhost:4000` and check it out. You should even be able to log in with your DJB credentials (if you are a member of the club) through OAuth.

## Contributing
If you would like to contribute to the site, please feel free to submit a pull request. If you have any questions, just ask one of the committers and we'll be glad to help you out.

For the best chance of success getting your pull request approved, please try to do the following few things:

1. Match the coding style of existing code as best as possible.
2. Write unit tests against your new code.
3. Document your work, or include updates to existing documentation.

[tarentum-website]: http://tarentum.org
[djb]: https://www.darkjedibrotherhood.com